import { Message } from './model/message';
import { User } from './model/user';
import { Room } from './model/room';
import socketIo = require('socket.io-client');
import fs = require('fs');
const NodeRSA = require('node-rsa');

const SERVER_URL = 'https://localhost:8080';
export class ChatClient {
  private socket: any;
  private keys: Map<string, any>;
  private readonly DEFAULT_KEY_PATH = '/tmp/keys/';

  constructor() {
    this.keys = new Map<string, any>();
    this.socket = socketIo(SERVER_URL, { rejectUnauthorized: false });
  }

  public register(name: string) {
    let user: User = new User();
    user.name = name;
    const key = this.generateKeyPair(name);
    user.publicKey = key.exportKey('public');
    this.socket.on('register', () => this.persistKey(name, key));
    this.socket.on('rejected', () => {
      console.log('Already registered. Please authenticate instead.');
      process.exit(1);
    })
    this.socket.emit('register', user);
  }

  public authenticate(name: string) {
    let user: User = new User();
    user.name = name;
    const key = this.importKeyFromFs(name);
    if (key == undefined) {
      console.log('no local key pair found');
      process.exit(1);
    }
    this.socket.on('authenticate', () => this.persistKey(name, key));
    this.socket.on('unknwon', () => {
      console.log('User not known to server. Register first.')
      process.exit(1);
    });
    this.socket.on('rejected', () => {
      console.log('Server has another public key for this user.')
      process.exit(1);
    });
    const data = { name: name, token: key.encryptPrivate('authenticate', 'base64') };
    this.socket.emit('authenticate', data);
  }

  public send(content: string, roomName: string): void {
    const encryptedContent = this.keys.get(roomName).encrypt(content, 'base64');
    let message: Message = new Message(encryptedContent, this.keys.get(roomName).exportKey('public'), roomName);
    this.socket.emit('message', message);
  }

  public listen(): void {
    this.socket.on('message', (m: Message) => console.log('In room', m.roomName + ': ' + this.keys.get(m.roomName).decrypt(m.content, 'utf-8')));
    this.socket.on('room', (r: Room) => {
      if(!this.keys.has(r.name)) {
        const key = this.importKeyFromFs(r.name);
        this.persistKey(r.name, key);
      }
    });
    this.socket.on('join', (data: {user: User, room: Room}) => console.log(data.user.name ,'joined', data.room.name));
  }

  private generateKeyPair(name: string): any {
    let key = new NodeRSA();
    key.generateKeyPair();
    return key;
  }

  private importKeyFromFs(name: string): any {
    let key: any = new NodeRSA();
    const basePath = this.DEFAULT_KEY_PATH + name;
    if(fs.existsSync(basePath) && fs.existsSync(basePath + '.pub')) {
      const publicKey: string = fs.readFileSync(basePath + '.pub', 'utf-8');
      const privateKey: string = fs.readFileSync(basePath, 'utf-8');
      key.importKey(privateKey);
      key.importKey(publicKey);
      return key;
    } else {
      return undefined;
    }
  }

  private persistKey(name: string, key: any): void {
    this.keys.set(name,key);

    const publicKey = key.exportKey('public');
    const privateKey = key.exportKey('private');
    const basePath = this.DEFAULT_KEY_PATH + name;
    fs.writeFileSync(basePath, privateKey, 'utf-8');
    fs.writeFileSync(basePath + '.pub', publicKey, 'utf-8');
  }
}

// const client = new ChatClient();
