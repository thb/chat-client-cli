import { User } from './user';
import { Message } from './message';

export class Room {
  name: string;
  publicKey: string;
  participants?: User[];
  messages?: Message[];

  constructor(name: string, publicKey: string) {
    this.name = name;
    this.publicKey = publicKey;
  }
}

