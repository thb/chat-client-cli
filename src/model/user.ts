import { Room } from './room';

export class User {
  name?: string;
  publicKey?: string;
  rooms?: Room[];
}

