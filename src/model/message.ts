import { Room } from './room';

export class Message {
  content: string;
  publicKey: string;
  roomName: string;

  constructor(content: string, publicKey: string, roomName: string) {
    this.content = content;
    this.publicKey = publicKey;
    this.roomName = roomName;
  }
}
