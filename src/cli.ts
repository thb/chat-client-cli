import { ChatClient } from './client';
const readline = require('readline');

export class CLI {
  private client?: ChatClient;
  private rl: any;

  public run() {
    this.client = new ChatClient();
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    this.rl.question('register or authenticate\n> ', (choice: string) => {
      this.rl.question('enter the user name\n> ', (name: string) => {
        if(choice == 'register') {
          this.client!.register(name);
        } else if (choice == 'authenticate') {
          this.client!.authenticate(name);
        } else {
          console.log('unknown choice');
          this.rl.close();
          process.exit(1);
        }
        this.client!.listen();
        console.log('To send messages follow the scheme <room: message\\n>');
        this.rl.on('line', (input: string) => {
          const room = input.substring(0, input.indexOf(': '));
          const content = input.substring(input.indexOf(': ')+1);
          this.client!.send(content, room);
        });
      });
    });
  }
}

let cli = new CLI();
cli.run();
